## Setup
```
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
```

## Running Development Server

```
$ python manage.py runserver
```
