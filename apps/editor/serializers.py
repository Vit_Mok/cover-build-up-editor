from rest_framework import serializers

from .models import CBU


class CBUSerializer(serializers.Serializer):
    """ Сериализатор CBU """
    unit = serializers.IntegerField(min_value=0)
    reach = serializers.ListField(
        child=serializers.FloatField(
            min_value=0,
            max_value=100,
        ),
        min_length=10,
        max_length=10
    )

    def validate_reach(self, value):
        """ Проверка значений охватов """
        if sorted(value, reverse=True) != value:
            raise serializers.ValidationError('Значения охватов должны убывать.')
        for reach in value:
            if reach % 1 == 0:
                raise serializers.ValidationError('Значения охватов должны быть дробными.')
        return value

    def create(self, validated_data):
        reach_list = validated_data['reach']
        cbu = CBU.objects.create(
            unit=validated_data['unit'],
            reach1=reach_list[0],
            reach2=reach_list[1],
            reach3=reach_list[2],
            reach4=reach_list[3],
            reach5=reach_list[4],
            reach6=reach_list[5],
            reach7=reach_list[6],
            reach8=reach_list[7],
            reach9=reach_list[8],
            reach10=reach_list[9],
            user=self.context['request'].user
        )
        return cbu
