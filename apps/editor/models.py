from django.db import models
from django.contrib.auth.models import User


class CBU(models.Model):
    """ CBU таблица """
    unit = models.PositiveIntegerField(verbose_name='Unit')
    reach1 = models.FloatField(verbose_name='Reach% 1+')
    reach2 = models.FloatField(verbose_name='Reach% 2+')
    reach3 = models.FloatField(verbose_name='Reach% 3+')
    reach4 = models.FloatField(verbose_name='Reach% 4+')
    reach5 = models.FloatField(verbose_name='Reach% 5+')
    reach6 = models.FloatField(verbose_name='Reach% 6+')
    reach7 = models.FloatField(verbose_name='Reach% 7+')
    reach8 = models.FloatField(verbose_name='Reach% 8+')
    reach9 = models.FloatField(verbose_name='Reach% 9+')
    reach10 = models.FloatField(verbose_name='Reach% 10+')
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='cbu',
        verbose_name='Пользователь'
    )

    class Meta:
        db_table = 'cbu'
