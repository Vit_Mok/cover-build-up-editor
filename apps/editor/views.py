from rest_framework import viewsets, status
from rest_framework.response import Response

from .models import CBU
from .serializers import CBUSerializer


class CBUView(viewsets.ViewSet):
    """ Ввод и вывод таблицы CBU """

    def list(self, request):
        cbu_objects = CBU.objects.filter(user=self.request.user)
        queryset = []
        for cbu in cbu_objects:
            queryset.append(
                {
                    "unit": cbu.unit,
                    "reach": [cbu.reach1, cbu.reach2, cbu.reach3, cbu.reach4, cbu.reach5,
                              cbu.reach6, cbu.reach7, cbu.reach8, cbu.reach9, cbu.reach10]
                }
            )
        serializer = CBUSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = CBUSerializer(
            data=self.request.data,
            context={'request': self.request},
            many=True
        )
        if serializer.is_valid():
            self.request.user.cbu.all().delete()
            serializer.save()
            return Response(data=self.request.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)